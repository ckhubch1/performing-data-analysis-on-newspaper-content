package fileProcessDecorator.fileOperations;

public abstract class FileProcessorAbstractBase
{
	public abstract void processFileData(InputDetails inputDetailsIn);

}
