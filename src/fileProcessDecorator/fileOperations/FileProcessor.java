package fileProcessDecorator.fileOperations;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

public class FileProcessor
{
	FileWriter write = null;

	public String readFile(String fileName) throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try
		{
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null)
			{
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally
		{
			br.close();
		}
	}

	public void writeFile(ArrayList<String> List1, ArrayList<String> List2, ArrayList<String> List3,
			Hashtable<String, Integer> List4, String fileName)
	{
		String wordFreq = "";
		int val = 1;
		try
		{
			if (val == 1)
			{
				FileWriter clear = new FileWriter(fileName);
				clear.write("");
				clear.close();
				val = 0;
			}
			write = new FileWriter(fileName, true);
			write.write("---DECORATOR_PARAGRAPH_DECORATOR_START---\n");
			for (int i = 0; i < List1.size(); i++)
			{
				write.write(List1.get(i) + "\n");
			}
			write.write("---DECORATOR_PARAGRAPH_DECORATOR_END---\n\n\n");
			write.write("---DECORATOR_SENTENCE_DECORATOR_START---\n");
			for (int i = 0; i < List2.size(); i++)
			{
				write.write(List2.get(i) + "\n");
			}
			write.write("---DECORATOR_SENTENCE_DECORATOR_END---\n\n\n");
			write.write("---DECORATOR_WORD_DECORATOR_START---\n");
			for (int i = 0; i < List3.size(); i++)
			{
				write.write(List3.get(i) + "\n");
			}
			write.write("---DECORATOR_WORD_DECORATOR_END---\n\n\n");
			write.write("---DECORATOR_WORD_FREQUENCY_DECORATOR_START---\n");
			Set<String> keys = List4.keySet();
			for (String key : keys)
			{
				wordFreq += (key + " : " + List4.get(key) + "\n");
			}
			write.write(wordFreq + "\n");
			write.write("---DECORATOR_WORD_FREQUENCY_DECORATOR_END---");
			write.close();
		}

		catch (IOException e)
		{
			System.err.println("Error while writing to output.txt");
		}

	}

}
