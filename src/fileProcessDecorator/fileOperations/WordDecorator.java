package fileProcessDecorator.fileOperations;

public class WordDecorator extends FileProcessorAbstractBase
{
	FileProcessorAbstractBase FPAB;

	public WordDecorator(FileProcessorAbstractBase FP)
	{
		FPAB = FP;
	}

	public void processFileData(InputDetails InputDetailsIn)
	{
		for (int i = 0; i < InputDetailsIn.Sentences.size(); i++)
		{
			String line = InputDetailsIn.Sentences.get(i);
			String[] arr = line.split(" ");
			for (int j = 0; j < arr.length; j++)
			{
				InputDetailsIn.Words.add(arr[j]);
			}
		}
		System.out.println("---DECORATOR_WORD_DECORATOR_START---");
		for (int i = 0; i < InputDetailsIn.Words.size(); i++)
		{
			System.out.print(InputDetailsIn.Words.get(i) + "\n");
		}
		System.out.println("---DECORATOR_WORD_DECORATOR_END---\n");
		FPAB.processFileData(InputDetailsIn);
	}
}
