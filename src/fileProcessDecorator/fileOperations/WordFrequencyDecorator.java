package fileProcessDecorator.fileOperations;

import java.util.Set;

public class WordFrequencyDecorator extends FileProcessorAbstractBase
{
	FileProcessorAbstractBase FPAB;

	public WordFrequencyDecorator(FileProcessorAbstractBase FP)
	{
		FPAB = FP;
	}

	public void processFileData(InputDetails InputDetailsIn)
	{
		String wordFreq = "";
		for (int i = 0; i < InputDetailsIn.Words.size(); i++)
		{
			if (InputDetailsIn.WordHashtable.containsKey(InputDetailsIn.Words.get(i)))
			{
				int count = InputDetailsIn.WordHashtable.get(InputDetailsIn.Words.get(i)) + 1;
				InputDetailsIn.WordHashtable.put(InputDetailsIn.Words.get(i), count);
			}

			else
			{
				InputDetailsIn.WordHashtable.put(InputDetailsIn.Words.get(i), 1);
			}
		}
		System.out.println("---DECORATOR_WORD_FREQUENCY_DECORATOR_START---");
		Set<String> keys = InputDetailsIn.WordHashtable.keySet();
		for (String key : keys)
		{
			wordFreq += (key + " : " + InputDetailsIn.WordHashtable.get(key) + "\n");
		}
		System.out.println(wordFreq);
		System.out.println("---DECORATOR_WORD_FREQUENCY_DECORATOR_END---");
	}

}
