package fileProcessDecorator.fileOperations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

public class InputDetails
{
	public ArrayList<String> Paragraphs = new ArrayList<String>();
	public ArrayList<String> Sentences = new ArrayList<String>();
	public ArrayList<String> Words = new ArrayList<String>();
	public String inputFile;
	public Hashtable<String, Integer> WordHashtable = new Hashtable<>();

	public String getDetails()
	{
		FileProcessor FPObj = new FileProcessor();
		try
		{
			inputFile = FPObj.readFile("input.txt");
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return inputFile;
	}
}

/*
 * for (int i = 0; i < count; i++) { line = FPObj.readLine();
 * System.out.println(line); parts.add(line.split("(?<=\\.  )"));
 * Paragraphs.add(parts[0]); Paragraphs.add(parts[1]); } FPObj.CloseFile();
 * for(int i = 0; i<Paragraphs.size(); i++)
 * System.out.println(Paragraphs.get(i)+"\n");
 * 
 */
