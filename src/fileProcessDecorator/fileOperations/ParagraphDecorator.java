package fileProcessDecorator.fileOperations;

public class ParagraphDecorator extends FileProcessorAbstractBase
{
	FileProcessorAbstractBase FPAB;

	public ParagraphDecorator(FileProcessorAbstractBase FP)
	{
		FPAB = FP;
	}

	public void processFileData(InputDetails InputDetailsIn)
	{
		String inputFile = InputDetailsIn.getDetails();
		String[] arr = inputFile.split("(\\.  )|(\\.[\n])");
		for (int i = 0; i < arr.length; i++)
		{
			arr[i] += ".";
			InputDetailsIn.Paragraphs.add(arr[i]);
		}
		System.out.println("---DECORATOR_PARAGRAPH_DECORATOR_START---");
		for (int i = 0; i < InputDetailsIn.Paragraphs.size(); i++)
		{
			System.out.print(InputDetailsIn.Paragraphs.get(i) + "\n");
		}
		System.out.println("---DECORATOR_PARAGRAPH_DECORATOR_END---\n");
		FPAB.processFileData(InputDetailsIn);
	}
}
