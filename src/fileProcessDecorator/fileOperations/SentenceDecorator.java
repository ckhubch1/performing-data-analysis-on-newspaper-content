package fileProcessDecorator.fileOperations;

public class SentenceDecorator extends FileProcessorAbstractBase
{
	FileProcessorAbstractBase FPAB;

	public SentenceDecorator(FileProcessorAbstractBase FP)
	{
		FPAB = FP;
	}

	public void processFileData(InputDetails InputDetailsIn)
	{
		
		for(int i =0; i<InputDetailsIn.Paragraphs.size();i++)
		{
			String line = InputDetailsIn.Paragraphs.get(i);
			String [] arr = line.split("(?<=\\. )");
			for(int j = 0; j<arr.length; j++)
			{
				InputDetailsIn.Sentences.add(arr[j]);
			}
		}
		System.out.println("---DECORATOR_SENTENCE_DECORATOR_START---");
		for (int i = 0; i < InputDetailsIn.Sentences.size(); i++)
		{
			System.out.print(InputDetailsIn.Sentences.get(i) + "\n");
		}
		System.out.println("---DECORATOR_SENTENCE_DECORATOR_END---\n");
		FPAB.processFileData(InputDetailsIn);
	}

	
}
