package fileProcessDecorator.driver;

import java.io.IOException;

import fileProcessDecorator.fileOperations.FileProcessor;
import fileProcessDecorator.fileOperations.FileProcessorAbstractBase;
import fileProcessDecorator.fileOperations.InputDetails;
import fileProcessDecorator.fileOperations.ParagraphDecorator;
import fileProcessDecorator.fileOperations.SentenceDecorator;
import fileProcessDecorator.fileOperations.WordDecorator;
import fileProcessDecorator.fileOperations.WordFrequencyDecorator;

public class Driver
{

	public static void main(String args[]) throws IOException
	{
		if (args.length != 2 || !args[0].equals("input.txt") || !args[1].equals("output.txt") )
		{
			System.err.println("Error: Check command line argument");
			System.exit(0);
		}
		FileProcessor FPObj = new FileProcessor();
		InputDetails inputDetails = new InputDetails();

		FileProcessorAbstractBase wordFrequencyDecorator = new WordFrequencyDecorator(null);
		FileProcessorAbstractBase wordDecorator = new WordDecorator(wordFrequencyDecorator);
		FileProcessorAbstractBase sentenceDecorator = new SentenceDecorator(wordDecorator);
		FileProcessorAbstractBase paragraphDecorator = new ParagraphDecorator(sentenceDecorator);

		paragraphDecorator.processFileData(inputDetails);

		FPObj.writeFile(inputDetails.Paragraphs, inputDetails.Sentences, inputDetails.Words, inputDetails.WordHashtable,
				"output.txt");

	}
}
